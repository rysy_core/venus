# venus
Slightly modified [venus](https://kvakil.github.io/venus/) for Rysy Core.

## Using venus

This version of venus is [available online](http://rysy_core.gitlab.io/venus/).

## Building

### Prerequests on Windows Subsystem for Linux

```
sudo apt install npm
sudo apt install openjdk-8-jdk
sudo update-alternatives --config java
npm install
npm install -g grunt
./gradlew build
```

### Build

```
./gradlew build && grunt dist
```

Results in `out/index.html`.

## Resources

#### [User Guide](https://github.com/kvakil/venus/wiki)

#### [MIT License](https://github.com/kvakil/venus/blob/master/LICENSE)
